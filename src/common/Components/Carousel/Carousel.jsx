import { FreeMode, Navigation, Thumbs } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import CarouselItem from "./Carouseltem";
import { twMerge } from "tailwind-merge";
import { LoadingSkeleton } from "../Loading";
import { memo, useCallback, useLayoutEffect, useState } from "react";
import { isEmpty } from "lodash";

const Carousel = ({
  gallery,
  isLoop = true,
  isThumbs = false,
  isLoading,
  breakpoints,
  thumbsBreakpoints,
  className,
  thumbsClassName,
  contentClassName,
}) => {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  const [slidesPerView, setSlidesPerView] = useState(null);
  const [thumbsSlidesPerView, setThumbsSlidesPerViewThumbs] = useState(null);

  const getSkeletonCount = useCallback((width, breakpointsValue) => {
    const sortedBreakpoints = Object.keys(breakpointsValue)
      .map(Number)
      .sort((a, b) => a - b);
    let slidesPerViewValue = 1;

    for (let i = sortedBreakpoints.length - 1; i >= 0; i--) {
      if (width >= sortedBreakpoints[i]) {
        slidesPerViewValue = breakpointsValue[sortedBreakpoints[i]].slidesPerView;
        break;
      }
    }

    return slidesPerViewValue;
  }, []);

  useLayoutEffect(() => {
    const handleResize = () => {
      const slideCount = getSkeletonCount(window.innerWidth, breakpoints);
      setSlidesPerView(slideCount);

      if (!isEmpty(thumbsBreakpoints)) {
        const slideCountThumbs = getSkeletonCount(window.innerWidth, thumbsBreakpoints);
        setThumbsSlidesPerViewThumbs(slideCountThumbs);
      }
    };

    window.addEventListener("resize", handleResize);
    handleResize();
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [breakpoints, getSkeletonCount, thumbsBreakpoints]);

  if (isLoading) {
    return (
      <>
        <div
          className={twMerge("grid w-full gap-3", className)}
          style={{ gridTemplateColumns: `repeat(${slidesPerView}, minmax(0, 1fr))` }}
        >
          {Array.from({ length: slidesPerView }).map((_, index) => {
            return <LoadingSkeleton key={index} className={contentClassName} />;
          })}
        </div>
        {Array.from({ length: thumbsSlidesPerView }).map((_, index) => {
          return <LoadingSkeleton key={index} className={thumbsClassName} />;
        })}
      </>
    );
  }

  return (
    <div className={twMerge("w-full", className)}>
      <Swiper
        loop={isLoop}
        spaceBetween={12}
        navigation={true}
        {...(isThumbs && {
          thumbs: { swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null },
        })}
        modules={[FreeMode, Navigation, Thumbs]}
        breakpoints={breakpoints}
      >
        {gallery.map((data, index) => (
          <SwiperSlide key={index}>
            <CarouselItem data={data} className={contentClassName} />
          </SwiperSlide>
        ))}
      </Swiper>

      {isThumbs && (
        <Swiper
          {...(isThumbs && {
            onSwiper: setThumbsSwiper,
          })}
          spaceBetween={12}
          freeMode={true}
          watchSlidesProgress={true}
          modules={[FreeMode, Navigation, Thumbs]}
          breakpoints={thumbsBreakpoints}
        >
          {gallery.map((data, index) => (
            <SwiperSlide key={index}>
              <CarouselItem data={data} className={thumbsClassName} />
            </SwiperSlide>
          ))}
        </Swiper>
      )}
    </div>
  );
};

export default memo(Carousel);
